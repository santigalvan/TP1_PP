package dto;


public class DatosReporteDTO implements Comparable<DatosReporteDTO>
{
	private String nombrePersona;
	private String telefono;
	private String calle;
	private String altura;
	private String email;

	private String cumpleanios;
	private String tipoContacto;
	
	public DatosReporteDTO(String nombrePersona, String telefono, String calle, String altura, String email, String cumpleanios, String tipoContacto) 
	{
		this.nombrePersona = nombrePersona;
		this.telefono = telefono;
		this.calle = calle;
		this.altura = altura;
		this.email = email;
		this.cumpleanios = cumpleanios;
		this.tipoContacto = tipoContacto;
	}

	public String getNombrePersona() {
		return nombrePersona;
	}

	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCumpleanios() {
		return cumpleanios;
	}

	public void setCumpleanios(String cumpleanios) {
		this.cumpleanios = cumpleanios;
	}

	public String getTipoContacto() {
		return tipoContacto;
	}

	public void setTipoContacto(String tipoContacto) {
		this.tipoContacto = tipoContacto;
	}

	@Override
	public int compareTo(DatosReporteDTO e) 
	{
		String a = getCumpleanios();
        String b = e.getCumpleanios();
        String anioA = a.substring(0, 2);
        String anioB = b.substring(0, 2);
        return Integer.parseInt(anioA) - Integer.parseInt(anioB);
	}
}
