package dto;

public class DireccionDTO 
{
	private int idPersona;
	private int idDireccion;
	private int idLocalidad;
	private String calle;
	private String altura;
	private String piso;
	private String departamento;

	public DireccionDTO(int idDireccion, int idPersona, int idLocalidad, String calle, String altura, String piso, String departamento)
	{
		this.idDireccion = idDireccion;
		this.idPersona = idPersona;
		this.idLocalidad = idLocalidad;
		this.calle = calle;
		this.altura = altura;
		this.piso= piso;
		this.departamento= departamento;
	}

	public int getIdPersona() {
		return idPersona;
	}
	
	public int getIdDireccion() {
		return idDireccion;
	}
	public int getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdDireccion(int idDireccion) {
		this.idDireccion = idDireccion;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getAltura() {
		return altura;
	}

	public void setAltura(String altura) {
		this.altura = altura;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	@Override
	public String toString() 
	{
		return "[Calle: "+this.calle+", Altura: "+this.altura+", Piso: "+this.piso+", Departamento: "+this.departamento+"]";
	}
}