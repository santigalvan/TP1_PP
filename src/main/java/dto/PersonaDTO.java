package dto;

public class PersonaDTO 
{
	private int idPersona;
	private int idTipoContacto;
	private String nombre;
	private String telefono;
	private String cumpleanios;
	private String email;

	public PersonaDTO(int idPersona, int idTipoContacto, String nombre, String telefono, String cumpleanios, String email)
	{
		this.idPersona = idPersona;
		this.idTipoContacto = idTipoContacto;
		this.nombre = nombre;
		this.telefono = telefono;
		this.cumpleanios = cumpleanios;
		this.email = email;
	}
	
	public int getIdPersona() 
	{
		return idPersona;
	}
	
	public int getIdTipoContacto() 
	{
		return idTipoContacto;
	}

	public void setIdPersona(int idPersona)
	{
		this.idPersona = idPersona;
	}
	
	public String getNombre() 
	{
		return this.nombre;
	}

	public void setNombre(String nombre) 
	{
		this.nombre = nombre;
	}

	public String getTelefono() 
	{
		return this.telefono;
	}

	public void setTelefono(String telefono) 
	{
		this.telefono = telefono;
	}
	
	public String getCumpleanios() 
	{
		return cumpleanios;
	}

	public void setCumplanios(String cumplanios)
	{
		this.cumpleanios = cumplanios;
	}

	public String getEmail() 
	{
		return email;
	}

	public void setEmail(String email) 
	{
		this.email = email;
	}
	
	@Override 
	public String toString() {
		return "[Nombre y Apellido: "+nombre+", Telefono: "+ telefono+", Cumpleaños: "+cumpleanios+ ", Email: "+email+"]";
	}


}
