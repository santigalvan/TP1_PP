package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

public interface PersonaDAO 
{
	
	public boolean insert(PersonaDTO persona);
	
	public boolean update(PersonaDTO persona);

	public boolean delete(PersonaDTO persona_a_eliminar);
	
	public PersonaDTO selectPersona(int idPersona);
	
	public Integer max();
	
	public List<PersonaDTO> readAll();

	public boolean asociadoTipoContacto(int idTipoContacto);
}
