package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO
{
	private static final String insert = "INSERT INTO persona(idPersona, idTipoContacto, nombre, telefono, cumpleanios, email) VALUES(DEFAULT, ?, ?, ?, ?, ?)";
	private static final String selectPersona = "SELECT * FROM persona WHERE idPersona = ?";
	private static final String update = "UPDATE persona SET idTipoContacto = ?, nombre = ?, telefono = ?, cumpleanios = ?, email = ? WHERE idPersona = ?";
	private static final String delete = "DELETE FROM persona WHERE idPersona = ?";
	private static final String max = "SELECT IFNULL(MAX(idPersona), 0) AS resultado FROM persona";
	private static final String readall = "SELECT * FROM persona";
	private static final String exists = "SELECT EXISTS (SELECT idTipoContacto FROM persona where idTipoContacto = ?) as resultado";
	
	
	public Integer max()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(max);
			resultSet = statement.executeQuery();
		
			if(resultSet.next())
				return resultSet.getInt("resultado");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return -1;
	}
	
	public PersonaDTO selectPersona(int idPersona) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(selectPersona);
			statement.setInt(1, idPersona);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return new PersonaDTO(resultSet.getInt("idPersona"), resultSet.getInt("idTipoContacto"),resultSet.getString("Nombre"),
						resultSet.getString("Telefono"), resultSet.getString("Cumpleanios"), resultSet.getString("Email"));				
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean insert(PersonaDTO persona)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			statement.setInt(1, persona.getIdTipoContacto());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getCumpleanios());
			statement.setString(5, persona.getEmail());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean update(PersonaDTO persona)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setInt(1, persona.getIdTipoContacto());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getCumpleanios());
			statement.setString(5, persona.getEmail());
			statement.setInt(6, persona.getIdPersona());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(PersonaDTO persona_a_eliminar)
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getIdPersona()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public boolean asociadoTipoContacto(int idTipoContacto) {
		PreparedStatement statement;
		ResultSet resultSet ; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(exists);
			statement.setInt(1, idTipoContacto);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) {
				System.out.println("Exists: "+ resultSet.getInt("resultado"));
				if(resultSet.getInt("resultado") == 1) {
					return true;	
				}
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public List<PersonaDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				personas.add(new PersonaDTO(resultSet.getInt("idPersona"), resultSet.getInt("idTipoContacto"), resultSet.getString("Nombre"), resultSet.getString("Telefono"), resultSet.getString("Cumpleanios"), resultSet.getString("Email") ));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return personas;
	}
}
