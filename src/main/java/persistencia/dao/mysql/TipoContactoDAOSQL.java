package persistencia.dao.mysql;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import dto.TipoContactoDTO;
import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.TipoContactoDAO;

public class TipoContactoDAOSQL implements TipoContactoDAO
{
	private static final String insert = "INSERT INTO tipoContacto(idTipoContacto, nombre) VALUES(DEFAULT, ?)";
	private static final String selectTipoContacto = "SELECT * FROM tipoContacto WHERE idTipoContacto = ?";
	private static final String update = "UPDATE tipoContacto SET nombre = ? WHERE idTipoContacto = ?";
	private static final String delete = "DELETE FROM tipoContacto WHERE idTipoContacto = ?";
	private static final String readall = "SELECT * FROM tipoContacto";
	private static final String idTipoContacto = "SELECT idTipoContacto FROM tipoContacto WHERE nombre = ?";
	
	public int selectIdTipoContacto(String tipoContacto)
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(idTipoContacto);
			statement.setString(1, tipoContacto);
			resultSet = statement.executeQuery();
		
			if(resultSet.next())
				return resultSet.getInt("idTipoContacto");
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return -1;
	}
	
	public boolean insert(TipoContactoDTO tipoContacto)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(insert);
			//statement.setInt(1, tipoContacto.getIdTipoContacto());
			statement.setString(1, tipoContacto.getNombre());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public TipoContactoDTO selectTipoContacto(int idTipoContacto) 
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(selectTipoContacto);
			statement.setInt(1, idTipoContacto);
			resultSet = statement.executeQuery();
		
			if(resultSet.next()) 
			{
				return new TipoContactoDTO (resultSet.getInt("idTipoContacto"),resultSet.getString("Nombre"));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean update(TipoContactoDTO tipoContacto)
	{
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(update);
			statement.setString(1, tipoContacto.getNombre());
			statement.setInt(2, tipoContacto.getIdTipoContacto());
			if(statement.executeUpdate() > 0) //Si se ejecut� devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean delete(TipoContactoDTO tipoContacto_a_eliminar)
	{
		PreparedStatement statement;
		int chequeoUpdate = 0;
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(delete);
			statement.setString(1, Integer.toString(tipoContacto_a_eliminar.getIdTipoContacto()));
			chequeoUpdate = statement.executeUpdate();
			if(chequeoUpdate > 0) //Si se ejecutó devuelvo true
				return true;
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	
	public List<TipoContactoDTO> readAll()
	{
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<TipoContactoDTO> tipoContacto = new ArrayList<TipoContactoDTO>();
		Conexion conexion = Conexion.getConexion();
		try 
		{
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			
			while(resultSet.next())
			{
				tipoContacto.add(new TipoContactoDTO(resultSet.getInt("idTipoContacto"), resultSet.getString("nombre")));
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return tipoContacto;
	}
}
