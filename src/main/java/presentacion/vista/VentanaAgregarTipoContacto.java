package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.Controlador;

public class VentanaAgregarTipoContacto extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	//Datos de la persona
	private JTextField txtTipoContacto;

	private JButton btnConfirmarTipo;
	private Controlador controlador;

	public VentanaAgregarTipoContacto(Controlador controlador) 
	{
		super();
		setTitle("Agregar Tipo de Contacto");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 296, 158);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 284, 126);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Nuevo tipo: ");
		lblNombreYApellido.setBounds(10, 45, 113, 14);
		panel.add(lblNombreYApellido);
	
		txtTipoContacto = new JTextField();
		txtTipoContacto.setBounds(110, 42, 164, 20);
		panel.add(txtTipoContacto);
		txtTipoContacto.setColumns(10);
		
		btnConfirmarTipo = new JButton("Confirmar");
		btnConfirmarTipo.setBounds(93, 81, 122, 23);
		panel.add(btnConfirmarTipo);
		btnConfirmarTipo.addActionListener(this.controlador);
		
		this.setVisible(true);
	}
	
	public JTextField getTxtTipoContacto() 
	{
		return txtTipoContacto;
	}

	public JButton getBtnConfirmar() 
	{
		return btnConfirmarTipo;
	}
}

