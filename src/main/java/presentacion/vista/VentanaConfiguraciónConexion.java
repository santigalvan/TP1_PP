package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.Controlador;

public class VentanaConfiguraciónConexion extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTextField txtUsuario;
	private JTextField txtPassword;
	private JTextField txtIp;
	private JTextField txtPuerto;

	private JButton btnConfirmarConfiguracionConexion;
	private JButton btnSalir;
	
	private Controlador controlador;

	public VentanaConfiguraciónConexion(Controlador controlador) 
	{
		super();
		setTitle("Configurar Conexión BD");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 300, 380);
		contentPane = new JPanel();
		this.setResizable(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 300, 380);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblUsuario = new JLabel("Usuario: ");
		lblUsuario.setBounds(20, 100, 113, 14);
		panel.add(lblUsuario);
		
		JLabel lblPassword = new JLabel("Contraseña: ");
		lblPassword.setBounds(20, 140, 113, 14);
		panel.add(lblPassword);
	
		JLabel lblIp = new JLabel("Ip: ");
		lblIp.setBounds(20, 180, 113, 14);
		panel.add(lblIp);
		
		JLabel lblPuerto = new JLabel("Puerto");
		lblPuerto.setBounds(20, 220, 113, 14);
		panel.add(lblPuerto);
		
		txtUsuario = new JTextField("root");
		txtUsuario.setBounds(115, 98, 164, 20);
		panel.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		txtPassword = new JTextField("root");
		txtPassword.setBounds(115, 138, 164, 20);
		panel.add(txtPassword);
		txtPassword.setColumns(10);
		
		txtIp = new JTextField("localhost");
		txtIp.setBounds(115, 178, 164, 20);
		panel.add(txtIp);
		txtIp.setColumns(10);
		
		txtPuerto = new JTextField("3306");
		txtPuerto.setBounds(115, 218, 50, 20);
		panel.add(txtPuerto);
		txtPuerto.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 95, 279, 193);
		panel.add(panel_1);
		
		btnConfirmarConfiguracionConexion = new JButton("Confirmar");
		btnConfirmarConfiguracionConexion.setBounds(150, 310, 122, 23);
		panel.add(btnConfirmarConfiguracionConexion);
		btnConfirmarConfiguracionConexion.addActionListener(this.controlador);
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(20, 310, 122, 23);
		panel.add(btnSalir);
		btnSalir.addActionListener(this.controlador);
		
		
		this.setVisible(true);
	}
	
	public JButton getBtnConfirmar() 
	{
		return btnConfirmarConfiguracionConexion;
	}
	
	public JButton getBtnSalir()
	{
		return btnSalir;
	}

	public JTextField getTxtUsuario()
	{
		return txtUsuario;
	}

	public JTextField getTxtPassword()
	{
		return txtPassword;
	}

	public JTextField getTxtIp() 
	{
		return txtIp;
	}

	public JTextField getTxtPuerto() 
	{
		return txtPuerto;
	}


}

