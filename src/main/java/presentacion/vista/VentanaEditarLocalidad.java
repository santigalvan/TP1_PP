package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.Controlador;

public class VentanaEditarLocalidad extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	//Datos de la persona
	private JTextField txtLocalidad;

	private JButton btnConfirmar;
	private Controlador controlador;

	public VentanaEditarLocalidad(Controlador controlador, String nombre) 
	{
		super();
		setTitle("Editar Localidad");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 338, 156);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 333, 126);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Editar: ");
		lblNombreYApellido.setBounds(10, 45, 142, 14);
		panel.add(lblNombreYApellido);
	
		txtLocalidad = new JTextField(nombre);
		txtLocalidad.setBounds(141, 43, 180, 20);
		panel.add(txtLocalidad);
		txtLocalidad.setColumns(10);
		
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(93, 81, 125, 23);
		panel.add(btnConfirmar);
		btnConfirmar.addActionListener(this.controlador);
		
		this.setVisible(true);
	}
	
	public JTextField getTxtLocalidad() 
	{
		return txtLocalidad;
	}

	public JButton getBtnConfirmar() 
	{
		return btnConfirmar;
	}
}

