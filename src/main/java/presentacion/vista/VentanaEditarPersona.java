package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import observer.Observer;
import presentacion.controlador.Controlador;

public class VentanaEditarPersona extends JFrame implements Observer
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	//Datos de la persona
	private JTextField txtNombre;
	private JTextField txtTelefono;
	private JTextField txtCumpleanios;
	private JTextField txtEmail;
	
	private JComboBox<String> comboTipoContacto;
	//Datos del domicilio
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtPiso;
	private JTextField txtDepartamento;
	private JComboBox<String> comboLocalidad;
	
	private JButton btnConfirmarEditar;
	private JButton btnAgregarLocalidad;
	private JButton btnAgregarTipoContacto;
	private Controlador controlador;

	public VentanaEditarPersona(Controlador controlador, int idPersona) 
	{
		super();
		setTitle("Editando Contacto");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 684, 347);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(12, 0, 658, 249);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblAadirPersona = new JLabel("Datos de la persona");
		lblAadirPersona.setBounds(164, 24, 156, 14);
		panel.add(lblAadirPersona);
		
		JLabel lblDomicilio = new JLabel("Domicilio");
		lblDomicilio.setBounds(514, 24, 113, 14);
		panel.add(lblDomicilio);
		
		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 50, 143, 14);
		panel.add(lblNombreYApellido);
		
		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 80, 113, 14);
		panel.add(lblTelfono);
		
		JLabel lblCumpleanios = new JLabel("Cumpleaños");
		lblCumpleanios.setBounds(10, 110, 113, 14);
		panel.add(lblCumpleanios);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 140, 113, 14);
		panel.add(lblEmail);
		
		JLabel lblTipoContacto = new JLabel("Tipo de Contacto");
		lblTipoContacto.setBounds(10, 170, 126, 14);
		panel.add(lblTipoContacto);
		
		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(338, 50, 113, 14);
		panel.add(lblCalle);
		
		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(338, 80, 113, 14);
		panel.add(lblAltura);
		
		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(338, 110, 113, 14);
		panel.add(lblPiso);
		
		JLabel lblDepartamento = new JLabel("Departamento");
		lblDepartamento.setBounds(338, 140, 113, 14);
		panel.add(lblDepartamento);
		
		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(338, 170, 113, 14);
		panel.add(lblLocalidad);
		
		//Para traer los datos 0 = nombre, 1 = telefono, 2= calle, 3= altura, 4= piso, 5=departamento, 6=localidad, 7=email, 8=cumpleanios, 9=Tipo contacto
		String [] datosPersona = controlador.obtenerDatosPersona(idPersona);

		btnAgregarLocalidad = new JButton("+L");
		btnAgregarLocalidad.setBounds(482, 197, 89, 23);
		panel.add(btnAgregarLocalidad);
		btnAgregarLocalidad.addActionListener(this.controlador);
		
		btnConfirmarEditar = new JButton("Confirmar");
		btnConfirmarEditar.setBounds(266, 274, 154, 23);
		contentPane.add(btnConfirmarEditar);
		btnConfirmarEditar.addActionListener(this.controlador);
		
		btnAgregarTipoContacto = new JButton("+TC");
		btnAgregarTipoContacto.setBounds(106, 198, 89, 23);
		contentPane.add(btnAgregarTipoContacto);
		btnAgregarTipoContacto.addActionListener(this.controlador);
		
		txtNombre = new JTextField(datosPersona[0]);
		txtNombre.setBounds(156, 49, 164, 20);
		panel.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtTelefono = new JTextField(datosPersona[1]);
		txtTelefono.setBounds(156, 78, 164, 20);
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		txtCumpleanios = new JTextField(datosPersona[8]);
		txtCumpleanios.setBounds(156, 108, 164, 20);
		panel.add(txtCumpleanios);
		txtCumpleanios.setColumns(10);
		
		txtEmail = new JTextField(datosPersona[7]);
		txtEmail.setBounds(156, 138, 164, 20);
		panel.add(txtEmail);
		txtEmail.setColumns(10);
		
		comboTipoContacto = new JComboBox<>(controlador.listarTipos());
		comboTipoContacto.setSelectedItem(datosPersona[9]);
		comboTipoContacto.setBounds(156, 168, 164, 25);
		panel.add(comboTipoContacto);
		
		txtCalle = new JTextField(datosPersona[2]);
		txtCalle.setBounds(463, 48, 164, 20);
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField(datosPersona[3]);
		txtAltura.setBounds(463, 78, 164, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);
		
		txtPiso = new JTextField(datosPersona[4]);
		txtPiso.setBounds(463, 108, 164, 20);
		panel.add(txtPiso);
		txtPiso.setColumns(10);
		
		txtDepartamento = new JTextField(datosPersona[5]);
		txtDepartamento.setBounds(463, 138, 164, 20);
		panel.add(txtDepartamento);
		txtDepartamento.setColumns(10);
		
		comboLocalidad = new JComboBox<>(controlador.listarLocalidades());
		comboLocalidad.setSelectedItem(datosPersona[6]);
		comboLocalidad.setBounds(463, 168, 164, 25);
		panel.add(comboLocalidad);
			
		this.setVisible(true);
	}
	
	public JTextField getTxtNombre() 
	{
		return txtNombre;
	}

	public JTextField getTxtTelefono() 
	{
		return txtTelefono;
	}
	
	public JTextField getTxtCumpleanios() 
	{
		return txtCumpleanios;
	}
	
	public JTextField getTxtEmail() 
	{
		return txtEmail;
	}
	
	public String getTxtTipoContacto() 
	{
		return comboTipoContacto.getSelectedItem().toString();
	}
	
	public JTextField getTxtCalle() 
	{
		return txtCalle;
	}
	
	public JTextField getTxtAltura() 
	{
		return txtAltura;
	}
	
	public JTextField getTxtPiso() 
	{
		return txtPiso;
	}
	
	public JTextField getTxtDepartamento() 
	{
		return txtDepartamento;
	}

	public String getTxtLocalidad() 
	{
		return comboLocalidad.getSelectedItem().toString();
	}
	
	public JButton getBtnConfirmarEditar() 
	{
		return btnConfirmarEditar;
	}

	public JButton getBtnAgregarLocalidad() 
	{
		return btnAgregarLocalidad;
	}
	
	public JButton getBtnAgregarTipoContacto() 
	{
		return btnAgregarTipoContacto;
	}
	
	public void update() 
	{
		comboTipoContacto.removeAllItems();

		for(String tipoContacto: this.controlador.listarTipos()) 
			comboTipoContacto.addItem(tipoContacto);			
		
		comboLocalidad.removeAllItems();

		for(String localidad: this.controlador.listarLocalidades()) 
			comboLocalidad.addItem(localidad);			
	}
}

