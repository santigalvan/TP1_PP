package presentacion.vista;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import presentacion.controlador.Controlador;

public class VentanaEditarTipoContacto extends JFrame 
{
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	//Datos de la persona
	private JTextField txtTipoContacto;

	private JButton btnConfirmar;
	private Controlador controlador;

	public VentanaEditarTipoContacto(Controlador controlador, String nombre) 
	{
		super();
		setTitle("Editar Tipo de contacto");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 338, 156);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 333, 126);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNombreYApellido = new JLabel("Editar: ");
		lblNombreYApellido.setBounds(10, 45, 142, 14);
		panel.add(lblNombreYApellido);
	
		txtTipoContacto = new JTextField(nombre);
		txtTipoContacto.setBounds(141, 43, 180, 20);
		panel.add(txtTipoContacto);
		txtTipoContacto.setColumns(10);
		
		btnConfirmar = new JButton("Confirmar");
		btnConfirmar.setBounds(93, 81, 125, 23);
		panel.add(btnConfirmar);
		btnConfirmar.addActionListener(this.controlador);
		
		this.setVisible(true);
	}
	
	public JTextField getTxtTipoContacto() 
	{
		return txtTipoContacto;
	}

	public JButton getBtnConfirmar() 
	{
		return btnConfirmar;
	}
}

