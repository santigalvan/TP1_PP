package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;

import persistencia.conexion.Conexion;
import javax.swing.ImageIcon;
import java.awt.Insets;
import java.awt.Font;

public class Vista
{
	private JFrame frmAgenda;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnABMLocalidad;
	private JButton btnABMTipoContacto;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y apellido","Telefono", "Calle", "Altura", "Piso", "Departamento", "Localidad", "Email", "Cumpleaños", "Tipo de Contacto" };
	private JLabel lblAbmTipo;
	private JLabel lblAbmLocalidades;
	private JLabel lblAgregarContacto;
	private JLabel lblEditarcontacto;
	private JLabel lblEliminarContacto;
	private JLabel lblGenerarReporte;

	public Vista() 
	{
		super();
		initialize();
	}

	private void initialize() 
	{
		
		frmAgenda = new JFrame();
		frmAgenda.setTitle("Agenda");
		frmAgenda.setBounds(100, 100, 1179, 505);
		frmAgenda.setResizable(false);
		frmAgenda.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmAgenda.getContentPane().setLayout(null);
		
		lblAbmTipo = new JLabel("ABM - Tipo contacto");
		lblAbmTipo.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAbmTipo.setBounds(219, 450, 113, 14);
		frmAgenda.getContentPane().add(lblAbmTipo);
		
		lblGenerarReporte = new JLabel("Generar reporte");
		lblGenerarReporte.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblGenerarReporte.setBounds(830, 450, 92, 14);
		frmAgenda.getContentPane().add(lblGenerarReporte);
		
		lblEliminarContacto = new JLabel("Eliminar contacto");
		lblEliminarContacto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEliminarContacto.setBounds(708, 450, 98, 14);
		frmAgenda.getContentPane().add(lblEliminarContacto);
		
		lblAbmLocalidades = new JLabel("ABM - Localidades");
		lblAbmLocalidades.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAbmLocalidades.setBounds(346, 450, 102, 14);
		frmAgenda.getContentPane().add(lblAbmLocalidades);
		
		lblEditarcontacto = new JLabel("Editar contacto");
		lblEditarcontacto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblEditarcontacto.setBounds(598, 450, 86, 14);
		frmAgenda.getContentPane().add(lblEditarcontacto);
		
		lblAgregarContacto = new JLabel("Agregar contacto");
		lblAgregarContacto.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblAgregarContacto.setBounds(465, 450, 99, 14);
		frmAgenda.getContentPane().add(lblAgregarContacto);
		
		btnABMTipoContacto = new JButton("");
		btnABMTipoContacto.setBounds(250, 400, 50, 50);
		btnABMTipoContacto.setIcon(new ImageIcon(Vista.class.getResource("/img/tipoPersona.png")));
		btnABMTipoContacto.setContentAreaFilled(false);
		btnABMTipoContacto.setBorderPainted(false);
		frmAgenda.getContentPane().add(btnABMTipoContacto);
		
		btnAgregar = new JButton("");
		btnAgregar.setIcon(new ImageIcon(Vista.class.getResource("/img/botonAgregarPersona.png")));
		btnAgregar.setBounds(490, 400, 50, 50);
		btnAgregar.setContentAreaFilled(false);
		btnAgregar.setBorderPainted(false);
		frmAgenda.getContentPane().add(btnAgregar);
		
		btnReporte = new JButton("");
		btnReporte.setIcon(new ImageIcon(Vista.class.getResource("/img/Reportes.png")));
		btnReporte.setContentAreaFilled(false);
		btnReporte.setBorderPainted(false);
		btnReporte.setBounds(850, 400, 50, 50);
		frmAgenda.getContentPane().add(btnReporte);
		
		btnBorrar = new JButton("");
		btnBorrar.setIcon(new ImageIcon(Vista.class.getResource("/img/botonBorrarPersona.png")));
		btnBorrar.setBounds(730, 400, 50, 50);
		btnBorrar.setContentAreaFilled(false);
		btnBorrar.setBorderPainted(false);
		frmAgenda.getContentPane().add(btnBorrar);
		
		btnEditar = new JButton("");
		btnEditar.setIcon(new ImageIcon(Vista.class.getResource("/img/botonEditarPersona.png")));
		btnEditar.setBounds(610, 400, 50, 50);
		btnEditar.setContentAreaFilled(false);
		btnEditar.setBorderPainted(false);
		frmAgenda.getContentPane().add(btnEditar);
		
		btnABMLocalidad = new JButton("");
		btnABMLocalidad.setIcon(new ImageIcon(Vista.class.getResource("/img/Localidad.png")));
		btnABMLocalidad.setMargin(new Insets(0, 0, 0, 0));
		btnABMLocalidad.setContentAreaFilled(false);
		btnABMLocalidad.setBorderPainted(false);
		btnABMLocalidad.setBounds(370, 400, 50, 50);
		frmAgenda.getContentPane().add(btnABMLocalidad);
		
		JPanel panel = new JPanel();
		panel.setBounds(14, 77, 1134, 302);
		frmAgenda.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(0, 0, 1138, 306);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas); 
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon(Vista.class.getResource("/img/DiseñoAgenda2.png")));
		lblFondo.setBounds(-19, 0, 1219, 487);
		frmAgenda.getContentPane().add(lblFondo);
	}
	
	public void show()
	{
		this.frmAgenda.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frmAgenda.addWindowListener(new WindowAdapter() 
		{
			@Override
		    public void windowClosing(WindowEvent e) {
		        int confirm = JOptionPane.showOptionDialog(
		             null, "Estas seguro que quieres salir de la Agenda?", 
		             "Confirmacion", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		        	System.exit(0);
		        }
		    }
		});
		this.frmAgenda.setVisible(true);
	}
	
	public JButton getBtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getBtnEditar() 
	{
		return btnEditar;
	}
	
	public JButton getBtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getBtnReporte() 
	{
		return btnReporte;
	}
	
	public JButton getBtnABMLocalidad() 
	{
		return btnABMLocalidad;
	}
	
	public JButton getBtnABMTipoContacto() 
	{
		return btnABMTipoContacto;
	}
	
	public DefaultTableModel getModelPersonas() 
	{
		return modelPersonas;
	}
	
	public JTable getTablaPersonas()
	{
		return tablaPersonas;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}
}