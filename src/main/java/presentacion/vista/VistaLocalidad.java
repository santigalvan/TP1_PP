package presentacion.vista;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import observer.Observer;
import presentacion.controlador.Controlador;
import javax.swing.JLabel;

public class VistaLocalidad extends JFrame implements Observer
{	
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JTable tablaLocalidades;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private DefaultTableModel modelLocalidades;
	private  String[] nombreColumnas = {"Localidad" };

	private Controlador controlador;

	public VistaLocalidad(Controlador controlador) 
	{
		super();
		setTitle("ABM - Localidad");
		this.controlador = controlador;
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 290, 433);
		contentPane = new JPanel();
		this.setResizable(false);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		modelLocalidades = new DefaultTableModel(null, nombreColumnas);
		
		btnAgregar = new JButton("");
		btnAgregar.setBounds(21, 351, 50, 50);
		btnAgregar.setIcon(new ImageIcon(Vista.class.getResource("/img/botonAgregarLocalidad.png")));
		btnAgregar.setContentAreaFilled(false);
		btnAgregar.setBorderPainted(false);
		contentPane.add(btnAgregar);
		btnAgregar.addActionListener(this.controlador);

		btnBorrar = new JButton("");
		btnBorrar.setBounds(222, 351, 50, 50);
		btnBorrar.setIcon(new ImageIcon(Vista.class.getResource("/img/botonBorrarLocalidad.png")));
		btnBorrar.setContentAreaFilled(false);
		btnBorrar.setBorderPainted(false);
		contentPane.add(btnBorrar);
		btnBorrar.addActionListener(this.controlador);
		
		btnEditar = new JButton("");
		btnEditar.setBounds(118, 351, 50, 50);
		btnEditar.setIcon(new ImageIcon(Vista.class.getResource("/img/botonEditarLocalidad.png")));
		btnEditar.setContentAreaFilled(false);
		btnEditar.setBorderPainted(false);
		contentPane.add(btnEditar);
		btnEditar.addActionListener(this.controlador);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(21, 34, 250, 306);
		contentPane.add(spPersonas);
		tablaLocalidades = new JTable(modelLocalidades); 
		
		tablaLocalidades.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaLocalidades.getColumnModel().getColumn(0).setResizable(false);
		
		spPersonas.setViewportView(tablaLocalidades);
		
		JLabel lblLocalidades = new JLabel("Localidades");
		lblLocalidades.setBounds(103, 0, 121, 14);
		contentPane.add(lblLocalidades);
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setIcon(new ImageIcon(Vista.class.getResource("/img/diseñoEdit.png")));
		lblFondo.setBounds(0, 0, 294, 407);
		contentPane.add(lblFondo);
		
		this.setVisible(true);
	}
	
	public JButton getbtnAgregar() 
	{
		return btnAgregar;
	}

	public JButton getbtnBorrar() 
	{
		return btnBorrar;
	}
	
	public JButton getbtnEditar() 
	{
		return btnEditar;
	}
	
	public DefaultTableModel getModelLocalidades() 
	{
		return modelLocalidades;
	}
	
	public JTable getTablaLocalidades()
	{
		return tablaLocalidades;
	}

	public String[] getNombreColumnas() 
	{
		return nombreColumnas;
	}

	@Override
	public void update() 
	{
		this.controlador.refrescarLocalidades();
	}

}

